[SlimService][slim_service_link]
=======
<div style="display:inline-block">
<img src="https://pbs.twimg.com/profile_images/710555987032350723/GDHlxO_z.jpg" width="75" height="75" >
<img src="https://appdevelopermagazine.com/images/news_images/PPH7-App-Developer-Magazine_qnftmgpw.jpg" width="75" height="75" >
</div>

# A REST API based on SLIM Framework
  
 SlimService is an api created with Slim framework. [Slim framework][slim_link] is perfect for this as it has been specifically designed to build **REST APIs**.
 The objective of this api is to interact with the database of a multimedia library.
 Actually, the database used as a test contains only books
 You can find [the documentation of this api on Swagger][swagger_doc_link], an excellent tool to create api documentation in Json.
 Swagger can display all model/classes, filters routes by tags (developers,client,admins) and **you can even test the api directly from the documentation with the button 'Try it out'**
 
 ## Database Structure
 
 (The database script is included in the folder sql)
 The database behind the API contains 4 tables :
 - **book (id,name,image_url,category,author)** : Table containing all books
 - **member (id,name,avatar,member_since,nb_loans,nb_penality,locked)** : Table containing all members
 - **book_entity (id,available,nb_times_borrowed,item_status_rate,id_book)** : Table containing all book entities
 - **borrowed (id_loan,id_member,id_book,start_date,end_date,finished)** : Table containing all loans
 
 You can also find 5 triggers (Triggers manage stock and book availability in order to avoid user error) :
 - **after_insert_borrowed** : Triggered when a new loan is created (adjust book entity status and book stock)
 - **after_delete_borrowed** : Triggered when a loan is deleted (adjust book entity status and book stock)
 - **after_update_borrowed** : Triggered when a loan is deleted (adjust book entity status and book stock only if finished value has changed)
 - **after_insert_book_entity** : Triggered when a book entity is created (adjust book stock -> total and available)
 - **after_update_book_entity** : Triggered when a book entity is updated (adjust book stock if available column value has changed
 - **after_delete_book_entity** : Triggered when a book entity is deleted (adjust book stock)
 
 ## Project Structure
 
 Most of the files in this project belongs to SlimFramework. The Api files are the following:
 
 ### At the root
 
 - **index.php** : The main file of the framework. This file is automatically executed when you use framework functions so if you use files, classes or any other libraries to include , you have to mention them at the beginning of this file (before $app->run() command)
 Actually, this file also contains the function to return the connection to my database. You can move it, put it in another class, as you wish :)
 
 ### In folder 'public'
 
 - **memberManager.class.php** : A singleton class with methods to interact with **member** table
 - **bookManager.class.php** : A singleton class with methods to interact with **book** table
 - **entityManager.class.php** : A singleton class with methods to interact with **book_entity** table
 - **loanManager.class.php** : A singleton class with methods to interact with **borrowed table**
 
 ### In folder 'src'
 
 - **routes.php** : The file containing all path/routes used by the API and the data/headers returned. The function used to return data comes from my singleton classes.  (see **Routes section** below for more details).
 
 ## Routes
 
 - With SlimFramework , you can very easily customize routes/url for your API
 
 **Example :**
 ~~~~
     $app->group('/api',function() use ($app) { }
 ~~~~
 Between brackets, you can put all routes starting by myserver/public/api (check **routes.php** for more complete examples)
 
 You can also setup headers, token , status code return.
 
 **Example :**
 
 ~~~~
 $newResponse = $response->withJson(
                 bookManager::Instance()->getBooks($request),
                 200
             );
 ~~~~
 This function automatically set headers to **Content-Type: Json** and the status code returned to **200**.
 To customize headers, you can use [withHeader][with_header] function
 
 ## Installation
 
 Nothing more simple than copy/paste the project in one of your web server folder. Don't forget to import the sql script in your phpmyadmin :) The default URL access for your API will be : **myserver/slimservice/public/api/v1/???** or **myserver/slimservice/index.php/public/api/v1/???** depending of your apache server configuration. If you use XAMPP, the first one is the correct url.
 
 ### Customize your API base Url :
 
 To customize your API base URL, you have to change your Apache Server Settings :
 
 - Go to your apache folder and search for **httpd-vhosts.conf** file ( it sould be located in **apache/conf/extra** folder)
 - Edit the file by uncommenting **NameVirtualHost *:80**
 - Add the following code at the end of the file : (replace DocumentRoot, Directory and ServerName by yours)
 ~~~~
 <VirtualHost *:80>
     DocumentRoot "C:\xampp7\htdocs\slimservice\public"
     ServerName slimservice
   <Directory "C:\xampp7\htdocs\slimservice\public">
     Order allow,deny
     Allow from all
   </Directory>
 </VirtualHost>
 ~~~~
 In your hosts file , add the following line of code :
 ~~~~
 127.0.0.1 slimservice
 ~~~~
 
 Restart your apache server and now, you should be able to access your api by typing **http://slimservice/api/v1/????**
 
## Improvement
 
 Actually, this API is not complete, it can be improved by adding functionalities such as token,better error management... so feel free to fork this project and add your contribution...
 
 ## License
 
 This project is licensed under GNU 3.0
 
 
   [slim_link]: <https://www.slimframework.com>
   [with_header]: <https://www.slimframework.com/docs/objects/response.html#the-response-headers>
   [swagger_doc_link]: <https://app.swaggerhub.com/api/LostArchives/SlimServiceMediatheque/1.0.0>
   [slim_service_link]: <http://vps.lostarchives.fr/slimservice>
    
 
