<?php

/**
Singleton class to handle all queries related to the table 'borrowed'
**/

class loanManager {
	
    private static $instance = null;

    private function __construct()
    {
    }

    public static function Instance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new loanManager();
        }

        return self::$instance;
    }
	
    /*
    Add a new loan
    */
    public function addLoan($request) {

	$loan = json_decode($request->getBody()); // Loan data to add
		
	$sql = "INSERT INTO borrowed VALUES('',:id_member,:id_entity,:start_date,:end_date,:finished)";

	try {
			
            $db = getConnection();
            $stmt = $db->prepare($sql);
	    $stmt->bindParam("id_member",$loan->id_member);
	    $stmt->bindParam("id_entity",$loan->id_entity);
            $stmt->bindParam("start_date",$loan->start_date);
	    $stmt->bindParam("end_date",$loan->end_date);
	    $stmt->bindParam("finished",$loan->finished);
            $stmt->execute();
            $loan->id_loan = $db->lastInsertId(); // Will be used later in the client to check if query was successful
            $db = null;
            echo json_encode($loan);

        } catch (PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }

    }
	
     /*
     Edit a specific loan
     */
     public function editLoan($request) {

	$loan = json_decode($request->getBody()); // Updated loan data
	
        $id_loan = $request->getAttribute('id_loan'); // Id of the loan to edit
		
	$sql = "UPDATE borrowed SET start_date=:start_date,end_date=:end_date,finished=:finished WHERE id_loan=:id_loan";
	     
	try {
		    
            $db = getConnection();
            $stmt = $db->prepare($sql);
	    $stmt->bindParam("id_loan",$id_loan);
            $stmt->bindParam("start_date",$loan->start_date);
	    $stmt->bindParam("end_date",$loan->end_date);
	    $stmt->bindParam("finished",$loan->finished);
            $stmt->execute();
            $loan->id_loan = $id_loan; // Will be used later in the client to check if query was successful
            $db = null;
            echo json_encode($loan);
            

        } catch (PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }

    }
	
    /*
    Delete a specific loan
    */
    public function deleteLoan($request) {

        $id_loan = $request->getAttribute('id_loan'); // Id of the loan to delete
		
	$sql = "DELETE FROM borrowed WHERE id_loan=:id_loan";

	try {
		    
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("id_loan",$id_loan);
            $stmt->execute();
            $db = null;
            echo '1'; // If successs '1'
            

        } catch (PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }

    }
	

    

}


?>
