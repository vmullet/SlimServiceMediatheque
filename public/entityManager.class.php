<?php

/**
Singleton class to handle all queries related to book_entity table
**/

class entityManager {
	
	private static $instance = null;

    private function __construct()
    {
    }

    public static function Instance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new entityManager();
        }

        return self::$instance;
    }

    /*
    Get book_entites for a specific book
    */
    public function getEntities($response) {

	$id_book = $request->getAttribute('id_book'); // Id of the book for book_entities
		
        $sql = "Select * from book_entity where id_book=".$id_book;

        try {
            $stmt = getConnection()->query($sql);
            $wines = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            return json_encode($wines);
        } catch (PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
    }

    /*
    Add an entity for a specific book
    */
    public function addEntity($request) {

	$entity = json_decode($request->getBody()); //Entity data to add to the media library
	
        $id_book = $request->getAttribute('id_book'); //Id of the book for the book_entity

        $sql = "INSERT INTO book_entity VALUES(:id_entity,:available,:nb_times_borrowed,:item_status_rate,:id_book)";

        try {

			$db = getConnection();
            $stmt = $db->prepare($sql);
			$stmt->bindParam("id_entity",$entity->id_entity);
            $stmt->bindParam("available",$entity->available);
			$stmt->bindParam("nb_times_borrowed",$entity->nb_times_borrowed);
            $stmt->bindParam("item_status_rate",$entity->item_status_rate);
			$stmt->bindParam("id_book",$id_book);
            $stmt->execute();
            $entity->id = $db->lastInsertId();
            $db = null;
            echo json_encode($entity);

        } catch (PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }


    }

    /*
    Edit a specific book_entity
    */
    public function editEntity($request) {

        $entity = json_decode($request->getBody()); // Updated entity data
		
	$id_entity = $request->getAttribute('id_entity'); // Id of the entity to edit

        $sql = "UPDATE book_entity SET available=:available,nb_times_borrowed=:nb_times_borrowed,item_status_rate=:item_status_rate WHERE id=".$id_entity;

        try {
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("available",$entity->available);
			$stmt->bindParam("nb_times_borrowed",$entity->nb_times_borrowed);
            $stmt->bindParam("item_status_rate",$entity->item_status_rate);
            $stmt->execute();
            $entity->id = $db->lastInsertId();
            $db = null;
            echo json_encode($entity);


        } catch (PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }

    }

    /*
    Delete a specific entity
    */
    public function deleteEntity(\Slim\Http\Request $request) {

        $id_entity = $request->getAttribute('id_entity'); // Id of the entity to delete
        $sql = "DELETE FROM book_entity WHERE id=:id_entity";

        try {
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam('id_entity',$id_entity);
            $stmt->execute();
            $db = null;
            echo '1'; // If success , '1'

        } catch (PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
		
		
    }


}


?>
